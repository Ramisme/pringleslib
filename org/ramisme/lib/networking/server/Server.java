package org.ramisme.lib.networking.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import org.ramisme.lib.networking.client.Client;

/**
 * Basic and necessary server functions and utilities.
 * 
 * @author Ramisme
 * @since Jul 4, 2013
 */
public abstract class Server implements Runnable {
	private final int port;
	private ServerSocket server;

	/** Stores each client object. */
	private final List<Client> users = new ArrayList<Client>();

	public Server(final int port) {
		this.port = port;
	}

	/**
	 * Shutdown the server and all connections.
	 * 
	 * @throws IOException
	 */
	public void shutdown() throws IOException {
		this.server.close();
	}

	/**
	 * Return a list of users.
	 * 
	 * @return Client array
	 */
	public Client[] getUsers() {
		return this.users.toArray(new Client[this.users.size()]);
	}

	public void appendClient(final Client client) {
		synchronized (users) {
			this.users.add(client);
		}
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

}
