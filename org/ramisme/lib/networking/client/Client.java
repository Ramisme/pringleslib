package org.ramisme.lib.networking.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Basic and necessary functions and utilities for a client.
 * 
 * @author Ramisme
 * @since Apr 8, 2013
 */
public abstract class Client implements Runnable {
	private final String host;
	private final int port;

	private Socket socket;
	private InputStream inputStream;
	private OutputStream outputStream;

	public Client(final String host, final int port) {
		this.host = host;
		this.port = port;
	}

	/**
	 * Connect to the specified Socket.
	 * 
	 * @throws IOException
	 */
	public void connect() throws IOException {
		this.socket.connect(new InetSocketAddress(InetAddress.getByName(host),
				port));
		this.outputStream = this.socket.getOutputStream();
		this.inputStream = this.socket.getInputStream();
	}

	/**
	 * Disconnect the client from the current socket connection.
	 * 
	 * @throws IOException
	 */
	public void disconnect() throws IOException {
		this.socket.close();
		this.outputStream.close();
		this.inputStream.close();
	}

	public final String getHost() {
		return host;
	}

	public final int getPort() {
		return port;
	}

	public Socket getSocket() {
		return socket;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public OutputStream getOutputStream() {
		return outputStream;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

}
