package org.ramisme.lib.networking.packets;

/**
 * Top of the hierarchy. Holds the packet ID.
 * 
 * @author Ramisme
 * @since Apr 8, 2013
 */
public interface Packet {

	/**
	 * Return the packet's defining ID.
	 * 
	 * @return Packet ID
	 */
	public int getId();

}
