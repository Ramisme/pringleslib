package org.ramisme.lib.networking.packets.writeable;

import java.io.DataOutputStream;
import java.io.IOException;

import org.ramisme.lib.networking.packets.Packet;

/**
 * Extension of the Packet interface. Includes writing utility methods and
 * inherits the <tt>getId()</tt> method from <tt>Packet</tt>.
 * 
 * @author Ramisme
 * @since Apr 8, 2013
 */
public interface WriteablePacket extends Packet {

	/**
	 * Write information to the server.
	 * 
	 * @param outputStream
	 *            DataOutputStream using current socket connection.
	 * @throws IOException
	 */
	public void writePacketData(final DataOutputStream outputStream)
			throws IOException;

}
