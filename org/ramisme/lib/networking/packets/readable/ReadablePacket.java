package org.ramisme.lib.networking.packets.readable;

import java.io.DataInputStream;
import java.io.IOException;

import org.ramisme.lib.networking.packets.Packet;

/**
 * Extension of the Packet interface. Includes reading utility methods and
 * inherits the <tt>getId()</tt> method from <tt>Packet</tt>.
 * 
 * @author Ramisme
 * @since Apr 8, 2013
 */
public interface ReadablePacket extends Packet {

	/**
	 * Read information from the server.
	 * 
	 * @param inputStream
	 *            DataInputStream using current socket connection.
	 * @throws IOException
	 */
	public void readPacketData(final DataInputStream inputStream) throws IOException;

}
