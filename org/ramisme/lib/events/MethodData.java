package org.ramisme.lib.events;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Ramisme
 * @since Aug 11, 2013
 */
public final class MethodData {

	public final Object listener;
	public final Method method;
	public final Class<? extends Event> event;

	public MethodData(final Object listener, final Method method,
			final Class<? extends Event> event) {
		this.listener = listener;
		this.method = method;
		this.event = event;
	}

	public final void callEvent(final Event event) {
		try {
			method.invoke(listener, event);
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
