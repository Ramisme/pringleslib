package org.ramisme.lib.events;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ramisme
 * @since Jul 4, 2013
 */
public class EventManager {
	private final Map<Class<? extends Event>, MethodData[]> events = new HashMap<Class<? extends Event>, MethodData[]>();

	private static volatile EventManager instance;

	public static EventManager getInstance() {
		if (instance == null) {
			synchronized (EventManager.class) {
				if (instance == null) {
					instance = new EventManager();
				}
			}
		}
		return instance;
	}

	public void callEvent(final Event event) {
		if (events.get(event.getClass()) == null) {
			return;
		}

		for (final MethodData data : events.get(event.getClass())) {
			data.callEvent(event);
		}
	}

	public void registerEvent(final Object listener,
			final Class<? extends Event> event) {
		final List<MethodData> methodData = new ArrayList<MethodData>();
		for (final Method method : listener.getClass().getMethods()) {
			if (isEventMethod(method, event)) {
				methodData.add(new MethodData(listener, method, event));
			}
		}

		if (!events.containsKey(event)) {
			events.put(event,
					methodData.toArray(new MethodData[methodData.size()]));
		} else {
			final List<MethodData> addData = new ArrayList<MethodData>(
					methodData);
			addData.addAll(Arrays.asList(events.get(event)));
			events.put(event, addData.toArray(new MethodData[addData.size()]));
		}
	}

	public void unregisterEvent(final Class<? extends Event> event) {
		synchronized (events) {
			events.remove(event);
		}
	}
	
	private boolean isEventMethod(final Method method,
			final Class<? extends Event> event) {
		return method.isAnnotationPresent(EventHandler.class)
				&& event.isAssignableFrom(method.getParameterTypes()[0])
				&& method.getParameterTypes().length == 1;
	}
}
