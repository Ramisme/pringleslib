package org.ramisme.lib.events;

/**
 * Cancellable event implementation.
 * 
 * @author Ramisme
 * @since Jul 4, 2013
 */
public class Cancellable implements Event {
	private boolean cancelled;

	/**
	 * Returns whether the event is cancelled.
	 * 
	 * @return
	 */
	public boolean isCancelled() {
		return this.cancelled;
	}

	/**
	 * Sets the event to be cancelled.
	 * 
	 * @param flag
	 */
	public void setCancelled(final boolean flag) {
		this.cancelled = flag;
	}

}
