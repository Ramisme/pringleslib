package org.ramisme.lib.events;

/**
 * Upper-most class in the event hierarchy. Any class that wishes to be defined
 * as an event must implement this interface.
 * 
 * @author Ramisme
 * @since Jul 4, 2013
 */
public interface Event {

	public enum Priority {
		CRITICAL, HIGH, NORMAL, LOW, LOWEST;
	}

}
