package org.ramisme.lib.events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The EventHandler annotation is used to define which methods are to be invoked
 * as an event.
 * 
 * @author Ramisme
 * @since Mar 15, 2013
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {

	/**
	 * Event.Priority determines when the event will be called. By default, the
	 * priority is set to NORMAL and will run after CRITICAL and HIGH priority
	 * events, and before LOW and LOWEST priority events.
	 * 
	 * @return
	 */
	public Event.Priority priority() default Event.Priority.NORMAL;

}
