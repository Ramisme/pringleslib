package org.ramisme.lib.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * The URLManager is an implementation of the IOManager and is specifically
 * designed to read from a specified URL.
 * 
 * @author Ramisme
 * @since Mar 15, 2013
 */
public class URLManager implements IOManager {
	private HttpURLConnection connection;
	private BufferedReader bufferedReader;
	private final URL url;
	private Proxy proxy;

	private int timeout = 10000;

	/**
	 * Returns a new instance of the private URLManager object.
	 * 
	 * @param url
	 * @return
	 */
	public static URLManager newManager(final URL url) {
		return new URLManager(url);
	}

	/**
	 * The private URLManager class is an implementation of the IOManager
	 * interface and is specifically used for the sole purpose of reading lines
	 * from a specified URL.
	 * 
	 * @param url
	 */
	protected URLManager(final URL url) {
		this.url = url;
	}

	@Override
	public void setupReadStreams() {
		try {
			if (this.proxy != null) {
				connection = (HttpURLConnection) this.url.openConnection(proxy);
				connection.setConnectTimeout(this.getTimeout());
				connection.connect();
				bufferedReader = new BufferedReader(new InputStreamReader(
						connection.getInputStream()));
				return;
			}

			connection = (HttpURLConnection) url.openConnection();
			connection.addRequestProperty("User-Agent", "Mozilla/4.76");
			bufferedReader = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setupWriteStreams() {
		return;
	}

	@Override
	public void closeStreams() {
		if (bufferedReader != null) {
			try {
				bufferedReader.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}

		if (connection != null) {
			try {
				connection.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Return a string read from the specified URL.
	 * 
	 * @return
	 * @throws IOException
	 */
	public String readLine() throws IOException {
		return bufferedReader.readLine();
	}

	/**
	 * Get all results on the website very quickly and with ease.
	 * 
	 * @return <i>An array of strings that the webserver returned.</i>
	 * 
	 * @throws IOException
	 */
	public String[] getResults() throws IOException {
		String line;
		List<String> results = new ArrayList<String>();

		while ((line = readLine()) != null) {
			results.add(line);
			line = readLine();
		}

		return results.toArray(new String[results.size()]);
	}

	/**
	 * Return the current HttpURLConnection instance.
	 * 
	 * @return
	 */
	public HttpURLConnection getConnection() {
		return this.connection;
	}

	/**
	 * Set the current proxy object.
	 * 
	 * @param proxy
	 */
	public void setProxy(final Proxy proxy) {
		this.proxy = proxy;
	}

	/**
	 * Return the current proxy object.
	 * 
	 * @return
	 */
	public Proxy getProxy() {
		return this.proxy;
	}

	/**
	 * Set the current timeout variable.
	 * 
	 * @param timeout
	 */
	public void setTimeout(final int timeout) {
		this.timeout = timeout;
	}

	/**
	 * Return the current timeout.
	 * 
	 * @return
	 */
	public int getTimeout() {
		return this.timeout;
	}

}
