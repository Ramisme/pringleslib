package org.ramisme.lib.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Compare and match regex patterns to specified messages.
 * 
 * @author Ramisme
 * @since Jul 4, 2013
 */
public class RegexMatcher {
	private final String regex, match;
	private boolean matches;

	public static RegexMatcher match(final String regex, final String match) {
		final RegexMatcher matcher = new RegexMatcher(regex, match);
		matcher.compare();
		return matcher;
	}

	public RegexMatcher(final String regex, final String match) {
		this.regex = regex;
		this.match = match;
	}

	public void compare() {
		final Pattern pattern = Pattern.compile(regex);
		final Matcher matcher = pattern.matcher(match);
		this.matches = matcher.matches();
	}

	public boolean matches() {
		return this.matches;
	}

	@Override
	public String toString() {
		return String.format("regex[\"%s\"], match[\"%s\"], matches[%s]",
				this.regex, this.match, this.matches);
	}

}
