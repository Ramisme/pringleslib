package org.ramisme.lib.util;

import java.util.concurrent.TimeUnit;

/**
 * @author Ramisme
 * @since Jul 25, 2013
 */
public class TimeManager {

	/**
	 * The last variable is assigned to the last set time in nanoseconds.
	 */
	private long last;

	/**
	 * Return whether the amount of specified time has elapsed in milliseconds.
	 * 
	 * @param time
	 * @return
	 */
	public boolean sleep(final long time) {
		return this.sleep(time, TimeUnit.MILLISECONDS);
	}

	/**
	 * Return whether the amount of specified time has elapsed in the specified
	 * time unit converted from nanoseconds.
	 * 
	 * @param time
	 * @param timeUnit
	 * @return
	 */
	public boolean sleep(final long time, final TimeUnit timeUnit) {
		final long convert = timeUnit.convert(System.nanoTime() - last,
				TimeUnit.NANOSECONDS);
		if (convert >= time) {
			this.reset();
		}

		return convert >= time;
	}

	/**
	 * Reset the last time.
	 */
	public void reset() {
		this.last = System.nanoTime();
	}

}
