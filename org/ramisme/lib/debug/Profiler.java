package org.ramisme.lib.debug;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * The Profiler class is a debugging tool used to determine how long it takes
 * specific pieces of code to execute. To use this profiler, start a section in
 * the beginning of the code you wish to time, and end a section after the code
 * is finished executing. The user can define whether the profiler should print
 * out the return time or not.</p>
 * 
 * @author Ramisme
 * @since Mar 15, 2013
 */
public class Profiler {
	/**
	 * When a section is started, a <code>String</code> and a <code>Long</code>
	 * value is added to the <code>profilerMap</code> and will remain stored in
	 * the map until a section is ended or until the user requests a removal.
	 */
	private final Map<String, Long> profilerMap = new HashMap<String, Long>();

	private Logger<String> logger;

	private String currentSection;

	/**
	 * Determines whether information from the profiler should be logged to the
	 * console.
	 */
	private boolean isLogging;

	/**
	 * Singleton method to return a new instance of a normal profiler.
	 * 
	 * @return
	 */
	public static Profiler newProfiler() {
		return new Profiler();
	}

	/**
	 * Start a new section profiler
	 * 
	 * @param sectionName
	 */
	public void startSection(final String sectionName) {
		synchronized (profilerMap) {
			profilerMap.put(sectionName.toLowerCase(), getCurrentTimeMillis());
		}
	}

	/**
	 * Close the current section
	 */
	public void endSection() {
		this.endSection(this.currentSection);
	}

	private void endSection(final String sectionName) {
		synchronized (profilerMap) {
			if (this.profilerMap.containsKey(currentSection)) {
				if (this.isLogging()) {
					this.logSection(currentSection);
				}

				this.profilerMap.remove(currentSection);
			}
		}
	}

	private void logSection(final String sectionName) {
		this.logger.log(String.format(
				"Ended section: %s. Took %s milliseconds to execute.",
				sectionName, getTimeDifference(profilerMap.get(sectionName))));
	}

	/**
	 * Set the logging boolean.
	 * 
	 * @param flag
	 */
	public final void setLogging(final boolean logging) {
		this.isLogging = logging;
	}

	/**
	 * Returns the logging boolean to determine whether or not to log
	 * information.
	 * 
	 * @return
	 */
	public final boolean isLogging() {
		return this.isLogging;
	}

	public Logger<String> getLogger() {
		if (this.logger == null) {
			this.logger = new Logger<String>();
		}

		return this.logger;
	}

	/**
	 * Return the difference in milliseconds between a specified start time and
	 * the current time.
	 * 
	 * @param startTime
	 * @return
	 */
	private final long getTimeDifference(final long startTime) {
		return this.getTimeDifference(startTime, this.getCurrentTimeMillis());
	}

	/**
	 * Return the difference in milliseconds between a specified start time and
	 * a specified end time.
	 * 
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	private final long getTimeDifference(final long startTime,
			final long endTime) {
		return endTime - startTime;
	}

	/**
	 * The current system nanoTime divided by one million is the equivalent of
	 * the system time in milliseconds. This data is very useful for timing how
	 * long specific chunks of code take to execute.
	 * 
	 * @return
	 */
	private final long getCurrentTimeMillis() {
		return TimeUnit.MILLISECONDS.convert(System.nanoTime(),
				TimeUnit.NANOSECONDS);
	}

}
