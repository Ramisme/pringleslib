package org.ramisme.lib.debug;

/**
 * @author Ramisme
 * @since Jul 4, 2013
 */
public class Logger<T> {

	/**
	 * Print specified information using the specified category.
	 * 
	 * @param category
	 *            Log category
	 * @param data
	 *            Specified data
	 */
	public void log(final String category, final T data) {
		System.out
				.println(String.format("[%s]: %s", category, data.toString()));
	}

	/**
	 * Print specified information using the defeault "INFO" category.
	 * 
	 * @param data
	 *            Specified data
	 */
	public void log(final T data) {
		this.log("INFO", data);
	}

}
